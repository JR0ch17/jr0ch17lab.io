---
title: Media
comments: false
---

- Enterprise Security Magazine: **[Running a Successful Bug Bounty Program](https://network-security.enterprisesecuritymag.com/cxoinsight/running-a-successful-bug-bounty-program-nid-3562-cid-13.html)**
- HackerOne: **[Ten Practical Tips For High-Value Pentest Engagements](https://www.hackerone.com/penetration-testing/ten-practical-tips-high-value-pentest-engagements)**
- Bugcrowd: **[How to Find Better Bugs](https://www.bugcrowd.com/resources/levelup/how-to-find-better-bugs-with-jr0ch17/)**
- Detectify: **[What’s the difference between Attack Surface Monitoring and Vulnerability Scanning?](https://blog.detectify.com/2021/10/21/whats-the-difference-between-attack-surface-monitoring-and-vulnerability-scanning/)**
- Detectify: **[Ethical hacker shares top tips to protect your attack surface](https://blog.detectify.com/2021/10/19/external-attack-surface-management/)**
- Landry Consulting: **[5 tips for a safe online Halloween for your kids](https://www.landryconsulting.com/en/blog/5-conseils-pour-une-fete-dhalloween-en-ligne-assurant-la-securite-vos-enfants/)**
- Bloomberg: **[Bugcrowd Report Highlights Need for Blend of Human Ingenuity and AI-Powered Security Solutions to Protect Critical Infrastructure](https://www.bloomberg.com/press-releases/2020-06-23/bugcrowd-report-highlights-need-for-blend-of-human-ingenuity-and-ai-powered-security-solutions-to-protect-critical)**
- Helpnet Security: **[Study of global hackers and the economics of security research](https://www.helpnetsecurity.com/2020/06/25/economics-of-security-research/)**
- The Register: **[Carbon-based vuln hunters will always be better at infosec than AI, insist puny humans](https://www.theregister.com/2020/06/24/bugcrowd_hacker_survey/)**
- Computer Weekly: **[Neurodiversity on the rise among career hackers](https://www.computerweekly.com/news/252484968/Neurodiversity-on-the-rise-among-career-hackers)**
- AP News: **[Bugcrowd Report Highlights Need for Blend of Human Ingenuity and AI-Powered Security Solutions to Protect Critical Infrastructure](https://apnews.com/press-release/business-wire/7b9003476a5e43bd9f74911de78d0ad0)**
- Security Informed: **[Bugcrowd Releases 2020 Inside The Mind Of A Hacker Report](https://www.securityinformed.com/news/bugcrowd-releases-2020-inside-the-mind-co-1592817701-ga.1592813094.html)**
- Bugcrowd: **[Demystifying Hackers: Bugcrowd’s 2020 Inside the Mind of a Hacker Report](https://itmoah.bugcrowd.com/)**
- Yahoo: **[Bugcrowd Report Highlights Need for Blend of Human Ingenuity and AI-Powered Security Solutions to Protect Critical Infrastructure](https://finance.yahoo.com/news/bugcrowd-report-highlights-blend-human-120000097.html)**
- TechNews: **[We need human ingenuity and AI-powered security](http://www.securitysa.com/10797r)**
- Security Week: **[Bug Hunters Confident They Will Continue to Outperform AI: Study
](https://www.securityweek.com/bug-hunters-confident-they-will-continue-outperform-ai-study)**
- Cyber News Group: **[Human Hackers Will Still Beat AI For A Further 10 Years! – & Their Demographics – Report](https://www.cybernewsgroup.co.uk/human-hackers-will-still-beat-ai-for-a-further-10-years-their-demographics-report/)**
- The Evolving Enterprise: **[Blend ingenuity and AI-based security to protect from hackers, says report](https://www.theee.ai/2020/06/24/3437-blend-ingenuity-and-ai-based-security-to-protect-from-hackers-says-report/)**
- SecureOps: **[Bug Bounty Programs are Becoming Increasingly Effective](https://secureops.com/blog/bug-bounty-programs-2/)**
- SecureOps: **[Unleashing Bug Bounty Programs](https://secureops.com/blog/bug-bounty-programs/)**
- Bugcrowd: **[Inside the Mind of a Hacker Interview](https://www.youtube.com/watch?v=SKwp4hITYH4)**
- Automotive News: **[Sleeping with the enemy](http://www.autonews.com/article/20181001/SHIFT/181009997/hacking-auto-electronic-systems)**
- (ISC)²: **[(ISC)² SSCP Spotlight](https://blog.isc2.org/isc2_blog/2017/09/sscp-spotlight-jasmin-landry.html)**




