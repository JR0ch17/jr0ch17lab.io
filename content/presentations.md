---
title: Presentations
subtitle: Presentations, Panel Discussions, Webinars and Podcasts
comments: false
---

- LanaudSec: **Bâtir un programme AppSec à partir de zéro**
- OWASP Toronto: **[EASM mistakes waiting to happen](https://www.youtube.com/watch?v=f5IEe5r9to8&ab_channel=OWASPNagpur)**
- OWASP Nagpur: **[Traversing my way in the internal network](https://www.youtube.com/watch?v=f5IEe5r9to8&ab_channel=OWASPNagpur)**
- YASCON 2020: **[Getting Blindly Lucky](https://www.youtube.com/watch?v=j7w0W2tx4Ms)**
- ITSP Magazine: **[Inside The Mind Of A Hacker Report | Unique Histories, Shared Destiny | A Bugcrowd Story](https://www.itspmagazine.com/their-stories/2020-inside-the-mind-of-a-hacker-report-unique-histories-shared-destiny-a-bugcrowd-story)**
- PolyHx: **[Qu'est-ce que le Bug Bounty?](https://www.youtube.com/watch?v=TN_F-3nRH0c)**
- c0c0n 2020: **[Capture the flag to secure career](https://www.youtube.com/watch?v=jItAWkM5xWA)**
- Detectify Hacker School 10: **[Finding 5 Bugs in a Single Request](https://detectify.wistia.com/medias/mctr80ng94)**
- HackerOne H@cktivitycon 2020: **[Beyond the Borders of Scope](https://www.youtube.com/watch?v=ZV7Xz8p12fo)**
- DomeHacKP 2020: **[Bountyful Bugs in Cyberspace](https://www.youtube.com/watch?v=Ddi4fV5f_Ss&ab_channel=DomeHacKP)**
- Bugcrowd: **[Managing The Paradigm Shift in Security Post-COVID](https://www.bugcrowd.com/resources/webinar/paradigm-shift-security/)**
- SecureOps: **[Understanding Bug Bounty and Pen Testing Programs](https://www.youtube.com/watch?v=3M09K0BGzTM)**
- Bugcrowd University: **[Burp Suite Advanced Part #1](https://www.youtube.com/watch?v=kbi2KaAzTLg&ab_channel=Bugcrowd)**
- ITSP Magazine: **[Why Do Bug Bounty Hunters Do What They Do? We Asked Them.](https://soundcloud.com/itspmagazine/why-do-bug-bounty-hunters-do-what-they-do-we-asked-them)**
- Bugcrowd LevelUp0x03: **[Bad API, hAPI Hackers!](https://www.youtube.com/watch?v=UT7-ZVawdzA&t=17s)**



